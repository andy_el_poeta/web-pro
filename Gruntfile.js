module.exports = function(grunt){
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt,{
        useminPrepare:'useminPrepare'
    })
    grunt.initConfig({
        sass:{
            dist:{
                files:[{
                    expand:true,
                    cwd:'css',
                    src:['*.scss'],
                    dest:'css',
                    ext:'.css'
                }]
            }
        },
        
        watch:{
            files:['css/*.scss'],
            tasks:['css']
        },
        browserSync:{
            dev:{
                bsFiles:{//browser files
                    src:[
                        "css/*.css",
                        ".*html",
                        "js/*.js"
                    ]

                },
            options:{
                watchTasks:true,
                server:{
                   baseDir:"./"//Directorio base de nuetro servidor
            }
           }
          }
        },
      imagemin:{
        dynamic:{
            files:[{
                expand:true,
                cwd:'./',
                scr:'img/*. {png,git,jpg,jepg}',
                dest:'dist/'
            }]
        }
    },
    copy:{
        html:{
            files:[{
            expand:true,
            dot:true,
            cwd:'./',
            src:['*.html'],
            dest:'dist'
        }]
       
    },
    fonts:{
        files:[
            {
            expand:true,
            dot:true,
            cwd:'node_modules/open-iconic/font',
            src:['fonts/*.*'],
            dest:'dist'
       
         }]
       }
    },
    clean:{
        build:{
            src:['dist/']
        }
    },
    cssmin:{
        dist:{}
    },
    uglify:{
        dist:{}
    },
    filerev:{
        options:{
            enconding:'utf8',
            algorithm:'md5',
            length:20
        },
        release:{
           //filerev:release hashes
           files:[{
               src:[
                   'dist/js/*.js',
                   'dist/css/*.css',
               ]
           }]
        }
    },
    concat:{
        options:{
            separator:';'
        },
        dist:{}
    },
    useminPrepare:{
        foo:{
            dest:'dist',
            src:['index.html, abaut.html,contacto.html,precios.html']
        },
        options:{
            flow:{
                steps:{
                   css:['cssmin'],
                   js:['uglify']
                
            },
            post:{
                css:[{
                    name:'cssmin',
                    createConfig:function(context,block){
                        var generated= context.options.generated;generated.options={
                            keepSpecialcomments:0,
                            rebase:false
                        }
                    }
                }]
            }
          }
       }
    },
    usemin: {
        html:['dist/index.html,dist/abaut.html,dist/precios.html,dist/contacto.html'],
        options:{
           assetsDir:['dist','dist/ css','dist/js']
         }
     }

   });
    
    grunt.registerTask('css',['sass']);
    grunt.registerTask('default',['browserSync','watch']);
    grunt.registerTask('img:compress',['imagemin']);
    grunt.registerTask('build',[
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'

    ])
};